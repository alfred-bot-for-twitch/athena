const fetch = require('node-fetch');
const LOG = require('../logger');

const adminSecret = process.env.HASURA_ADMIN_SECRET;
const hgeEndpoint = process.env.HGE_ENDPOINT;

function getHeaders() {
  return { 'Content-Type': 'application/json','x-hasura-admin-secret': adminSecret };
}

exports.update = async function(query, variables) {
  LOG.info(query);
  try {
    const resp = await fetch(`${hgeEndpoint}/v1/graphql`, {
      method: 'POST',
      body: JSON.stringify({
        query: query,
        variables: variables,
      }),
      headers: getHeaders(),
    });
    LOG.info(resp);
    return resp;
  } catch (error) {
    console.log(error);
  }
};
