const PLAYER_CAPACITY_FULL = `
    mutation update_players_full($channelId: String!, $playersNeeded: Boolean!){
        update_GAME_SESSION(where: {channel_id: {_eq: $channelId}},
            _set:{
                players_needed: $playersNeeded
            }){
                affected_rows
            }
    } 
`;

const UPDATE_PLAYER_STATUS = `
    mutation allow_player_to_play($channelId: String!, $userId: String!, $status: Boolean!) {
        update_PLAYER_SESSION(where: {channel_id: {_eq: $channelId}, user_id: {_eq: $userId}}, _set: {active: $status}) {
        affected_rows
        }
    }
`;

const INSERT_COMMAND_INTO_TABLE = `
    mutation inser_user_command($commands:  [USER_COMMANDS_insert_input!]!) {
        insert_USER_COMMANDS(objects: $commands) {
            affected_rows
        }
    }
`;

const DELETE_PLAYER_FROM_ALL_SESSIONS = `
    mutation remove_player_from_all_sessions($userId: String!) {
        delete_PLAYER_SESSION(where: {user_id: {_eq: $userId}}) {
        affected_rows
        }
        update_PLAYERS(where: {user_id: {_eq: $userId}}, _set: {expired: true, total_commands: 0}) {
        affected_rows
        }
    }
`;

const INCREMENT_COMMANDS_EXECUTED = `
    mutation increment_commands_executed($channelId: String!, $userId: String!) {
        update_PLAYER_SESSION(where: {channel_id: {_eq: $channelId}, user_id: {_eq: $userId}}, _inc: {commands_executed: 1}) {
        affected_rows
        }
    }  
`;

module.exports = {
  PLAYER_CAPACITY_FULL: PLAYER_CAPACITY_FULL,
  UPDATE_PLAYER_STATUS: UPDATE_PLAYER_STATUS,
  INSERT_COMMAND_INTO_TABLE: INSERT_COMMAND_INTO_TABLE,
  DELETE_PLAYER_FROM_ALL_SESSIONS: DELETE_PLAYER_FROM_ALL_SESSIONS,
  INCREMENT_COMMANDS_EXECUTED: INCREMENT_COMMANDS_EXECUTED
};
