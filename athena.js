const Redis = require('ioredis');
const {
  PLAYER_CAPACITY_FULL,
  UPDATE_PLAYER_STATUS,
  INSERT_COMMAND_INTO_TABLE,
  DELETE_PLAYER_FROM_ALL_SESSIONS,
  INCREMENT_COMMANDS_EXECUTED
} = require('./queries/mutations');
const { update } = require('./config/hasura');
const LOG = require('./logger');

const athena = function() {
  let redis = new Redis({
    port: process.env.MICRO_CACHE_PORT,
    host: process.env.MICRO_CACHE_HOST
  });
  const CHANNEL_POOL = 'channel_pool';
  const SESSION = 'session';
  const PLAYER = 'player';
  const COMMANDS_COUNT = 'COMMANDS_COUNT';

  async function addPlayerToChannelPool(playerJoinEvent) {
    let player = playerJoinEvent.data.new;
    try {
      await redis.sadd(
        `${CHANNEL_POOL}:${player.channel_id}`,
        `${PLAYER}:${player.user_id}`
      );
      //await redis.set(`${PLAYER}:${player.id}`, JSON.stringify(player), 'ex', player.time);
      await redis.set(`${PLAYER}:${player.user_id}`, player.total_commands);
      const commandsCount = await redis.get(`${PLAYER}:${player.user_id}:${COMMANDS_COUNT}`);
      if(!commandsCount){
        await redis.set(`${PLAYER}:${player.user_id}:${COMMANDS_COUNT}`, 0);
      }
      await updatePlayerActiveStatus(player.user_id, player.channel_id, true);
      const poolSize = await getChannelPoolSize(player.channel_id);
      const capacity = await getChannelCapacity(player.channel_id);
      if (poolSize === capacity) {
        await updateSessionPoolAvailability(player.channel_id, false);
      }
    } catch (error) {
      console.log(error);
    }
  }

  async function executePlayerCommands(command) {
    try {
      let playerCanExecute = await canPlayerExecuteCommand(command.user_id);
      LOG.info(`${command.user_id} can execute command ${playerCanExecute}`);
      if (playerCanExecute) {
        await insertCommandIntotable(command);
        await redis.incr(`${PLAYER}:${command.user_id}:${COMMANDS_COUNT}`);
        await update(INCREMENT_COMMANDS_EXECUTED, {
          channelId: command.channel_id,
          userId: command.user_id
        });
        playerCanExecute = await canPlayerExecuteCommand(command.user_id);
        LOG.info(`${command.user_id} can execute command ${playerCanExecute}`);
        if (!playerCanExecute)
          await removePlayerFromAllSessions(command.user_id);
        return;
      }
      await removePlayerFromAllSessions(command.user_id);
    } catch (error) {
      console.log(error);
    }
  }

  async function removePlayerFromAllSessions(userId){
    try {
      //await updatePlayerActiveStatus(player, false);
      const input = {
        userId: userId
      };
      await update(DELETE_PLAYER_FROM_ALL_SESSIONS, input);
    } catch (error) {
      console.log(error);
    }
  }

  async function canPlayerExecuteCommand(userId) {
    try {
      const commandsExecuted = await redis.get(
        `${PLAYER}:${userId}:${COMMANDS_COUNT}`
      );
      const totalCommands = await redis.get(`${PLAYER}:${userId}`);
      LOG.info(`${userId} has executed ${commandsExecuted} commands out of ${totalCommands}`);
      return Number(commandsExecuted) < Number(totalCommands); 
    } catch (error) {
      console.log(error);
    }
  }

  async function insertCommandIntotable(commands) {
    try {
      const qv = {
        commands: [commands],
      };
      const resp = await update(INSERT_COMMAND_INTO_TABLE, qv);
      return resp;
    } catch (error) {}
  }

  async function updateSessionPoolAvailability(channel_id, joiningAllowed) {
    try {
      const qv = {
        channelId: channel_id,
        playersNeeded: joiningAllowed,
      };
      const resp = await update(PLAYER_CAPACITY_FULL, qv);
      return resp;
    } catch (err) {
      console.log(err);
    }
  }

  async function updatePlayerActiveStatus(userId, channelId, status) {
    try {
      const qv = {
        userId: userId,
        status: status,
        channelId: channelId,
      };
      const resp = await update(UPDATE_PLAYER_STATUS, qv);
      if (!status) await deletePlayerFromSession(userId, channelId);
    } catch (err) {
      console.log(err);
    }
  }

  async function getChannelPoolSize(channelId) {
    let count = 0;
    try {
      count = await redis.scard(`${CHANNEL_POOL}:${channelId}`);
    } catch (err) {
      console.log(err);
    }
    return count;
  }

  async function getChannelCapacity(channelId) {
    let count = 0;
    try {
      let session = await getGameSessionInfo(channelId);
      if (session) count = session.player_capacity;
    } catch (err) {
      console.log(err);
    }
    return count;
  }

  async function getGameSessionInfo(channelId) {
    try {
      const session = await redis.get(`${SESSION}:${channelId}`);
      return JSON.parse(session);
    } catch (err) {
      console.log(err);
    }
  }

  async function createNewGameSession(newGameSessionEvent) {
    const newGameSession = {
      player_capacity: newGameSessionEvent.data.new.player_capacity,
    };

    try {
      await redis.set(
        `${SESSION}:${newGameSessionEvent.data.new.channel_id}`,
        JSON.stringify(newGameSession)
      );
    } catch (err) {
      console.log(err);
    }
  }

  async function deleteGameSession(channelId) {
    try {
      const players = await redis.smembers(`${CHANNEL_POOL}:${channelId}`);
      players.forEach(async member => {
        const playerId = member.split(':')[1];
        let player = await redis.get(`${PLAYER}:${channelId}:${playerId}`);
        await updatePlayerActiveStatus(player.user_id, player.channel_id, false);
      });
      await redis.del(`${SESSION}:${channelId}`);
    } catch (err) {
      console.log(err);
    }
  }

  async function deletePlayerFromSession(userId, channelId, removeCacheCount) {
    try {
      await redis.srem(
        `${CHANNEL_POOL}:${channelId}`,
        `${PLAYER}:${userId}`
      );
      if(removeCacheCount)
        await redis.del(`${PLAYER}:${userId}:${COMMANDS_COUNT}`);
    } catch (error) {}
  }

  // redis.on(event, (channel, message) => {
  //     const playerId = message.split(":")[1];
  //     try {
  //         await redis.srem(`${CHANNEL_POOL}:${player.channel_id}`, `${PLAYER}:${player.user_id}`);
  //         const player = await redis.get(`${PLAYER}:${player.user_id}`);
  //         await updatePlayerStatus(player, false);
  //         await updatePlayersJoiningSession(player, true);
  //     } catch(err){
  //         console.log(err);
  //     }
  // })

  return {
    getChannelPoolSize: getChannelPoolSize,
    getChannelCapacity: getChannelCapacity,
    addPlayerToChannelPool: addPlayerToChannelPool,
    createNewGameSession: createNewGameSession,
    deleteGameSession: deleteGameSession,
    executePlayerCommands: executePlayerCommands,
    deletePlayerFromSession: deletePlayerFromSession,
  };
};

module.exports = athena;
