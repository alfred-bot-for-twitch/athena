const logger = require('pino')();

const info = (params) => {
    if(Boolean(process.env.DEBUG))
        logger.info(params);
};


module.exports = {
    info: info
};
