const { ApolloServer, gql } = require('apollo-server-micro');
const { json } = require('micro');
const { router, post } = require('microrouter');
const athena = require('./athena')();
const LOG = require('./logger');

const PLAYER_GAME_JOIN_REQUEST = 'player_request';
const NEW_GAME_SESSION_REQUEST = 'new_game_session';
const PLAYER_LEAVE_GAME = 'player_game_leave';
const REMOVE_PLAYER = 'delete_player_from_game';
const GAME_STATUS_UPDATE = 'game_status_update';

const add_new_game = async (req, res) => {
  try {
    const data = await json(req);
    LOG.info(data);
    const hasuraEvent = data.event;
    if (data.trigger.name === NEW_GAME_SESSION_REQUEST) {
      await athena.createNewGameSession(hasuraEvent);
      res.end('success');
      return;
    }
    res.end('success');
  } catch (err) {
    console.log(err);
  }
};

const add_new_player = async (req, res) => {
  try {
    const data = await json(req);
    LOG.info(data);
    const hasuraEvent = data.event;
    if (data.trigger.name === PLAYER_GAME_JOIN_REQUEST) {
      const channelId = hasuraEvent.data.new.channel_id;
      const poolSize = await athena.getChannelPoolSize(channelId);
      const channelCapacity = await athena.getChannelCapacity(channelId);
      if (poolSize < channelCapacity) {
        await athena.addPlayerToChannelPool(hasuraEvent);
      }
    }
    res.end('success');
  } catch (err) {
    console.log(err);
  }
};

const player_leave_game = async (req, res) => {
  try {
    const data = await json(req);
    LOG.info(data);
    const hasuraEvent = data.event;
    if (data.trigger.name === PLAYER_LEAVE_GAME) {
      let player = hasuraEvent.data.new;
      if(!player){
        player = hasuraEvent.data.old;
        await athena.deletePlayerFromSession(player.user_id, player.channel_id, true);
        res.end('success');
      }
      if(!player.active){
        await athena.deletePlayerFromSession(player.user_id, player.channel_id);
        res.end('success');
      }
    }
  } catch (err) {
    console.log(err);
  }
};

const remove_player = async (req, res) => {
  try{
    const data = await json(req);
    LOG.info(data);
    const hasuraEvent = data.event;
    const player = hasuraEvent.data.old;
    if (data.trigger.name === REMOVE_PLAYER) {
      await athena.deletePlayerFromSession(player.user_id, player.channel_id, true);
    }
    res.end("success");
  } catch(err){
    console.log(err);
  }
}

const update_game_status = async (req, res) => {
  try{
    const data = await json(req);
    LOG.info(data);
    const hasuraEvent = data.event;
    const game = hasuraEvent.data.new;
    if (data.trigger.name === GAME_STATUS_UPDATE) {
      if(!game.active)
        await athena.deleteGameSession(game.channel_id);
      else
        await athena.createNewGameSession(hasuraEvent);
    }
    res.end("success");
  } catch(err){
    console.log(err);
  }
}

const typeDefs = gql`
  type Query {
    check: String
  }

  type Response {
    text: String
  }

  input CommandInput {
    command: String
    channel_id: String
    user_name: String
    user_id: String
  }

  type Mutation {
    EXECUTE_COMMAND(command: CommandInput): Response
  }
`;

const resolvers = {
  Mutation: {
    EXECUTE_COMMAND: async (parent, { command }) => {
      await athena.executePlayerCommands(command);
      return 'success';
    },
  },
  Query: {
    check: () => 'Success',
  },
};

const newCommandHanlder = new ApolloServer({
  typeDefs,
  resolvers,
}).createHandler({
  path: '/graphql',
});

module.exports = router(
  post('/newgame', add_new_game),
  post('/newplayer', add_new_player),
  post('/graphql', newCommandHanlder),
  post('/leavegame', player_leave_game),
  post('/removeplayer', remove_player),
  post('/gamestatus', update_game_status)
);
